#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "userprog/process.h"
#include "vm/page.h"
#include "filesys/filesys.h"
#include "filesys/cache.h"
#include "filesys/inode.h"

static void
syscall_handler (struct intr_frame *);
static bool
is_esp_valid (const void *);
static struct file_container *
find_file_cont_by_fd (int);
static void
remove_all_resouces (struct list *file_list);

void
syscall_init (void)
{
  lock_init (&syscall_lock);
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}

static void
syscall_handler (struct intr_frame *f UNUSED)
{
  // TJ 9.28
  /* check whether the esp is valid */
  if (!is_esp_valid (f->esp))
    exit (-1);

  uint32_t task = *((uint32_t*) f->esp);
#ifdef VM
  thread_current()->esp = f->esp;
#endif
  switch (task)
    {

    /* Halt the operating system. */
    case SYS_HALT:
      {
	halt ();
	break;
      }

      /* Terminate this process. */
    case SYS_EXIT:
      {
	if (!is_esp_valid ((void *) (f->esp + 4)))
	  exit (-1);

	int status = *(int *) (f->esp + 4);
	exit (status);
	break;
      }

      /* Start another process. */
    case SYS_EXEC:
      {
	void * v_cmd_line = (void *) (f->esp + 4);
	if (!is_esp_valid (v_cmd_line))
	  {
	    f->eax = -1;
	    return;
	  }

	char *cmd_line = *(uint32_t *) v_cmd_line;

	pid_t pid = process_execute (cmd_line);

	f->eax = pid;
	break;
      }

      /* Wait for a child process to die. */
    case SYS_WAIT:
      {
	if (!is_esp_valid ((void *) (f->esp + 4)))
	  exit (-1);

	pid_t pid = *(pid_t *) (f->esp + 4);

	/* Get the child ret status. */
	int ret = process_wait (pid);

	f->eax = ret;
	break;
      }

      /* Create a file. */
    case SYS_CREATE:
      {
	void * v_file_name = (void *) (f->esp + 4);
	char * file_name = *(uint32_t *) v_file_name;

	if (!is_esp_valid (v_file_name) ||
	!is_esp_valid ((void *)(f->esp + 8))||
	file_name == NULL)
	  {
	    exit (-1);
	  }

	uint32_t size = *(uint32_t *) (f->esp + 8);

	f->eax = create (file_name, size);
	break;
      }

      /* Delete a file. */
    case SYS_REMOVE:
      {
	void * v_file_name = (void *) (f->esp + 4);

	char *file_name = *(uint32_t *) v_file_name;

	if (!is_esp_valid (v_file_name) || file_name == NULL)
	  exit (-1);

	f->eax = remove (file_name);
	break;
      }

      /* Open a file. */
    case SYS_OPEN:
      {
	void * v_file_name = (void *) (f->esp + 4);

	char *file_name = *(uint32_t *) v_file_name;

	if (!is_esp_valid (v_file_name) || file_name == NULL)
	  exit (-1);

	f->eax = open (file_name);
	break;
      }

      /* Obtain a file's size. */
    case SYS_FILESIZE:
      {
	if (!is_esp_valid ((void *) (f->esp + 4)))
	  exit (-1);

	int fd = *(int *) (f->esp + 4);

	f->eax = filesize (fd);
	break;
      }

      /* Read from a file. */
    case SYS_READ:
      {
	if (!is_esp_valid ((void *) (f->esp + 4))
	    || !is_esp_valid ((void *) (f->esp + 8))
	    || !is_esp_valid ((void *) (f->esp + 12)))
	  {
	    exit (-1);
	  }

	int *fd = (int *) (f->esp + 4);
	void * v_buffer = (void *) (f->esp + 8);
	char * buffer = *(uint32_t *) v_buffer;

	if (!is_esp_valid ((void *) (buffer)) || get_user (buffer) == -1)
	  exit (-1);

	uint32_t size = *(uint32_t *) (f->esp + 12);

	f->eax = _read (*fd, buffer, size);
	break;
      }

      /* Write to a file. */
    case SYS_WRITE:
      {
	if (!is_esp_valid ((void *) (f->esp + 4))
	    || !is_esp_valid ((void *) (f->esp + 8))
	    || !is_esp_valid ((void *) (f->esp + 12)))
	  {
	    exit (-1);
	  }

	int *fd = (int *) (f->esp + 4);

	void * v_buffer = (void *) (f->esp + 8);
	char * buffer = *(uint32_t *) v_buffer;
	if (!is_esp_valid ((void *) (buffer)) || get_user (buffer) == -1)
	  exit (-1);

	uint32_t size = *(uint32_t *) (f->esp + 12);

	f->eax = _write (*fd, buffer, size);
	break;
      }

      /* Change position in a file. */
    case SYS_SEEK:
      {
	int *fd = (int *) (f->esp + 4);
	uint32_t* pos = (uint32_t *) (f->esp + 8);
	if (!is_esp_valid (fd) || !is_esp_valid (pos))
	  return;

	seek (*fd, *pos);
	break;
      }

      /* Report current position in a file. */
    case SYS_TELL:
      {
	int *fd = (int *) (f->esp + 4);
	if (!is_esp_valid (fd))
	  {
	    f->eax = -1;
	    return;
	  }

	f->eax = tell (*fd);
	break;
      }

      /* Close a file. */
    case SYS_CLOSE:
      {
	if (!is_esp_valid ((void *) (f->esp + 4)))
	  exit (-1);

	int fd = *(int *) (f->esp + 4);

	close (fd);
	break;
      }
    case SYS_CHDIR:
      {
	if (!is_esp_valid ((void *) (f->esp + 4)))
	  {
	    exit (-1);
	  }
	void * v_file_name = (void *) (f->esp + 4);
	char * file_name = *(uint32_t *) v_file_name;

	f->eax = chdir (file_name);
	break;
      }
    case SYS_MKDIR:
      {
	if (!is_esp_valid ((void *) (f->esp + 4)))
	  {
	    exit (-1);
	  }
	void * v_file_name = (void *) (f->esp + 4);
	char * file_name = *(uint32_t *) v_file_name;

	f->eax = mkdir (file_name);
	break;
      }
    case SYS_READDIR:
      {
	if (!is_esp_valid ((void *) (f->esp + 4)))
	  exit (-1);
	if (!is_esp_valid ((void *) (f->esp + 8)))
	  exit (-1);

	int fd = *(int *) (f->esp + 4);

	void * v_file_name = (void *) (f->esp + 8);
	char * file_name = *(uint32_t *) v_file_name;

	f->eax = readdir (fd, file_name);
	break;
      }
    case SYS_ISDIR:
      {
	if (!is_esp_valid ((void *) (f->esp + 4)))
	  exit (-1);
	int fd = *(int *) (f->esp + 4);
	f->eax = isdir (fd);
	break;
      }
    case SYS_INUMBER:
      {
	if (!is_esp_valid ((void *) (f->esp + 4)))
	  exit (-1);
	int fd = *(int *) (f->esp + 4);
	f->eax = inumber (fd);
	break;
      }
#ifdef VM

      /* Map a file into memory. */
      case SYS_MMAP:
	{
	  if (!is_esp_valid ((void *) (f->esp + 4))
	      || !is_esp_valid ((void *) (f->esp + 8)))
	  exit (-1);

	  int fd = *(uint32_t *) (f->esp + 4);
	  void * virtual_addr = *(uint32_t *) (f->esp + 8);

	  f->eax = file_mmap(fd, virtual_addr);
	  break;
	}

      /* Remove a memory mapping. */
      case SYS_MUNMAP:
	{
	  if (!is_esp_valid ((void *) (f->esp + 4)))
	  exit (-1);

	  mapid_t mapping_ID = *(uint32_t *) (f->esp + 4);
	  munmap(mapping_ID);

	  break;
	}

      /* Exit the thread with unmapped call */
#endif

    default:
      exit (-1);
    }

}

static bool
is_esp_valid (const void * esp)
{
  if (esp < PHYS_BASE && esp >= CODE_SEG_BASE)
    return true;
  else
    return false;
}

void
halt (void)
{
  shutdown_power_off ();
}

void
exit (int status)
{
  struct thread *cur = thread_current ();

#ifdef VM
  munmap_all ();
#endif

  printf ("%s: exit(%d)\n", cur->name, status);
  if (list_size (&cur->file_con_list) != 0)
    {
      remove_all_resouces (&cur->file_con_list);
    }

  cur->ret_status = status;

  thread_exit ();
}

bool
create (const char *file_name, unsigned initial_size)
{
  //lock_acquire (&syscall_lock);
  bool success = filesys_create (file_name, initial_size, false);
  //lock_release (&syscall_lock);

  return success;
}

bool
remove (const char *file_name)
{
  bool is_removed = filesys_remove (file_name);
  return is_removed;
}

int
open (const char *file_name)
{
  struct file *file = filesys_open (file_name);
  if (file == NULL)
    return -1;
  //lock_acquire (&syscall_lock);

  struct file_container * f_cont = (struct file_container *) malloc (
      sizeof(struct file_container));
  f_cont->file = file;
  f_cont->is_being_mapped = false;
  f_cont->is_closed = false;

  size_t size = list_size (&thread_current ()->file_con_list);

  if (size == 0)
    {
      f_cont->fd = 2;
      thread_current ()->fd = 2;
    }
  else
    {
      thread_current ()->fd++;
      f_cont->fd = thread_current ()->fd;
    }

  list_push_back (&thread_current ()->file_con_list, &f_cont->elem);
  int fd = f_cont->fd;

  //lock_release (&syscall_lock);

  return fd;
}

int
filesize (int fd)
{
  struct file_container *fc = find_file_cont_by_fd (fd);
  struct file * file = fc->file;
  if (file == NULL)
    return -1;

  //lock_acquire (&syscall_lock);
  int size = file_length (file);
  //lock_release (&syscall_lock);

  return size;
}

int
_read (int fd, char *buffer, unsigned size)
{
  if (fd == STDIN_FILENO)
    {
      //lock_acquire (&syscall_lock);

      int i = 0;
      for (i = 0; i < size; i++)
	{
	  buffer[i] = input_getc ();
	}

      //lock_release (&syscall_lock);
      return size;
    }

  struct file_container * file_con = find_file_cont_by_fd (fd);

  if (file_con == NULL)
    return -1;

  struct file * file = file_con->file;
  if (inode_isdir (file_get_inode (file_con->file)))
    {
      return -1;
    }

  //lock_acquire (&syscall_lock);
  int off_set = file_read (file, buffer, size);
  //lock_release (&syscall_lock);
  return off_set;
}

int
_write (int fd, const void *buffer, unsigned size)
{
  if (fd == STDOUT_FILENO)
    {
      //ddlock_acquire (&syscall_lock);
      putbuf (buffer, size);
      //lock_release (&syscall_lock);

      return size;
    }

  struct file_container * file_con = find_file_cont_by_fd (fd);

  if (file_con == NULL)
    return -1;

  struct file * file = file_con->file;

  if (inode_isdir (file_get_inode (file_con->file)))
    {
      return -1;
    }
  //lock_acquire (&syscall_lock);
  int _size = file_write (file, buffer, size);
  //lock_release (&syscall_lock);

  return _size;
}

void
seek (int fd, unsigned position)
{
  struct file_container *file_container = find_file_cont_by_fd (fd);
  if (file_container == NULL)
    return;

  //lock_acquire (&syscall_lock);
  file_seek (file_container->file, position);
  //lock_release (&syscall_lock);
}

unsigned
tell (int fd)
{
  struct file_container *file_container = find_file_cont_by_fd (fd);
  if (file_container == NULL)
    {
      return -1;
    }

  //lock_acquire (&syscall_lock);

  int off_size = file_tell (file_container->file);

  //lock_release (&syscall_lock);

  return off_size;
}

void
close (int fd)
{
  struct file_container* file_con = find_file_cont_by_fd (fd);

  if (file_con->is_being_mapped)
    {
      file_con->is_closed = true;
      return;
    }

  if (file_con != NULL)
    {
      //lock_acquire (&syscall_lock);
      file_close (file_con->file);
      //lock_release (&syscall_lock);

      list_remove (&file_con->elem);
      free (file_con);
    }
}

bool
chdir (const char *dir)
{
  return dir_change_dir_to (dir);
}

bool
mkdir (const char *dir)
{
  return filesys_create (dir, 0, true);
}

bool
readdir (int fd, char *name)
{
  struct file_container *file_container = find_file_cont_by_fd (fd);

  if (file_container == NULL)
    return false;

  struct inode* inode = file_get_inode (file_container->file);

  if (inode_isdir (inode))
    {
      return dir_readdir ((struct dir *) file_container->file, name);
    }

  return false;
}

bool
isdir (int fd)
{
  struct file_container *file_container = find_file_cont_by_fd (fd);

  if (file_container == NULL)
    return false;

  struct inode* inode = file_get_inode (file_container->file);
  if (inode == NULL)
    return false;

  return inode_isdir (inode);
}

int
inumber (int fd)
{
  struct file_container *file_container = find_file_cont_by_fd (fd);

  if (file_container == NULL)
    return -1;

  struct inode* inode = file_get_inode (file_container->file);
  return inode_get_inumber (inode);
  return -1;
}

#ifdef VM

mapid_t
file_mmap(int fd, void *virtual_addr)
  {
    /* In the following cases, we do not map the file. */
    int file_length = filesize (fd);

    if (fd == 0 || fd == 1 || file_length <= 0 || virtual_addr == NULL)
    return -1;

    /* 1. Check if it is valid.
     -- Is it page aligned */
    if (pg_round_down (virtual_addr) != virtual_addr)
    return -1;

    /* 2. Finish all the parts.
     --NOTE: the last part need to be zeroed. */
    struct file_container * file_con = find_file_cont_by_fd (fd);
    struct file * file = file_con->file;
    file_seek (file, 0);
    off_t ofs = 0;
    int read_bytes = file_length;
    int zero_bytes = 0;

    /* 3. Assign a map ID. */
    thread_current ()->map_id++;
    mapid_t mapping_ID = thread_current ()->map_id;

    while (read_bytes > 0 || zero_bytes > 0)
      {
	/* Calculate how to fill this page.
	 We will read PAGE_READ_BYTES bytes from FILE
	 and zero the final PAGE_ZERO_BYTES bytes. */
	size_t page_read_bytes = read_bytes < PGSIZE ? read_bytes : PGSIZE;
	size_t page_zero_bytes = PGSIZE - page_read_bytes;

	bool success = page_entry_load (file, ofs, virtual_addr, page_read_bytes,
	    page_zero_bytes, !(file->deny_write),
	    MMAP);
	if (!success)
	  {
	    /* Free all possible blocks. */
	    munmap (mapping_ID);
	    return -1;
	  }
	else
	  {
	    /* add this page into mmap_container */
	    struct mmap_container * mmap_container =
	    (struct mmap_container *) malloc (sizeof(struct mmap_container));

	    mmap_container->fd = fd;
	    mmap_container->mapping_ID = mapping_ID;
	    mmap_container->page_entry = find_page_by_address (virtual_addr);
	    list_push_back (&thread_current ()->mmap_container_list,
		&mmap_container->elem);
	  }

	/* Advance. */
	read_bytes -= page_read_bytes;
	zero_bytes -= page_zero_bytes;

	// TJ 10.26
	/* If the file is not sequentially read anymore,
	 the following code is very important.  */
	ofs += page_read_bytes;
	virtual_addr += PGSIZE;
      }

    /* 4. Return the map ID. */
    file_con->is_being_mapped = true;
    return mapping_ID;
  }

/* Unmap all the files. */
void
munmap_all ()
  {
    mapid_t mapping_ID = 0;
    struct list *all_mmap_cont_list = &thread_current ()->mmap_container_list;
    struct list_elem *e;
    for (e = list_begin (all_mmap_cont_list);
	e != list_end (all_mmap_cont_list) && !list_empty (all_mmap_cont_list);)
      {
	struct mmap_container *mmap_container = list_entry(e,
	    struct mmap_container,
	    elem);
	mapping_ID = mmap_container->mapping_ID;

	while(e != list_end (all_mmap_cont_list) )
	  {
	    struct mmap_container *_mmap_container = list_entry(
		e, struct mmap_container, elem);

	    if(_mmap_container->mapping_ID != mapping_ID)
	    break;

	    e = list_next (e);
	  }

	munmap(mapping_ID);
      }
  }

/* Unmap a file by the mapping_ID. */
void
munmap (mapid_t mapping_ID)
  {
    int fd = -1;

    struct list *all_mmap_cont_list = &thread_current ()->mmap_container_list;
    struct list_elem *e;
    for (e = list_begin (all_mmap_cont_list);
	e != list_end (all_mmap_cont_list) && !list_empty (all_mmap_cont_list);)
      {
	struct mmap_container *mmap_container = list_entry(e,
	    struct mmap_container,
	    elem);
	if (mmap_container->mapping_ID == mapping_ID)
	  {
	    fd = mmap_container->fd;

	    /* 2. If it is dirty, write back the dirty file. */
	    if (mmap_container->page_entry->is_loaded)
	      {
		if (pagedir_is_dirty (thread_current ()->pagedir,
			mmap_container->page_entry->virtual_addr))
		  {
		    //lock_acquire (&syscall_lock);
		    file_write_at (mmap_container->page_entry->file,
			mmap_container->page_entry->virtual_addr,
			mmap_container->page_entry->read_bytes,
			mmap_container->page_entry->ofs);
		    //lock_release (&syscall_lock);
		  }

		/* Get the kernel address. */
		void * kernel_addr = pagedir_get_page (
		    thread_current ()->pagedir,
		    mmap_container->page_entry->virtual_addr);

		/* Find the frame. */
		struct frame* frame = find_frame_by_kernel_addr(kernel_addr);
		/* Destroy the frame(container). */
		frame_destroy(frame);

		/* Clear frame(physical memory). */
		pagedir_clear_page (thread_current ()->pagedir,
		    mmap_container->page_entry->virtual_addr);
	      }

	    e = list_remove (&mmap_container->elem);
	    free_page_by_address (mmap_container->page_entry->virtual_addr);
	    mmap_container->page_entry = NULL;
	    free (mmap_container);
	    continue;
	  }

	e = list_next (e);
      }

    /* 3. If the file is closed during the mapping, close it. */

    struct file_container *file_cont = find_file_cont_by_fd (fd);

    if (file_cont != NULL && file_cont->is_being_mapped && file_cont->is_closed)
      {
	file_cont->is_being_mapped = false;
	file_cont->is_closed = false;
	close (fd);
      }

  }
#endif

/* This function is to find a certain container by the
 file descriptor. If not found, return NULL. */
static struct file_container *
find_file_cont_by_fd (int fd)
{
  struct list *file_list = &thread_current ()->file_con_list;
  struct list_elem *e;
  for (e = list_begin (file_list); e != list_end (file_list); e = list_next (e))
    {
      struct file_container *f = list_entry(e, struct file_container, elem);
      if (fd == f->fd)
	{
	  return f;
	}
    }

  return NULL;
}

/* This function is to release all the resources the thread holds. */
static void
remove_all_resouces (struct list *file_list)
{
  struct list_elem *e;
  for (e = list_begin (file_list); e != list_end (file_list);)
    {
      struct file_container *file_con = list_entry(e, struct file_container,
						   elem);
      //lock_acquire (&syscall_lock);
      file_close (file_con->file);
      //lock_release (&syscall_lock);

      e = list_remove (&file_con->elem);
      free (file_con);
    }
}

/* Reads a byte at user virtual address UADDR.
 UADDR must be below PHYS_BASE.
 Returns the byte value if successful, -1 if a segfault
 occurred. */
int
get_user (const uint8_t *uaddr)
{
  int result;
  asm ("movl $1f, %0; movzbl %1, %0; 1:"
      : "=&a" (result) : "m" (*uaddr));
  return result;
}
