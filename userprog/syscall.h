#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H

#include <list.h>
#include <user/syscall.h>
#include "filesys/file.h"

#define REMOVE_ALL_MAP 0

// TJ 9.30
/* This is for the synchronization purpose  */
struct lock syscall_lock;

/* This is to record the files that current thread opens. */
struct file_container
  {
    int fd;
    struct file* file;
    struct list_elem elem;
    bool is_being_mapped;
    bool is_closed;
  };

void syscall_init (void);

void halt(void);
void exit(int status);
void munmap_all ();

#endif /* userprog/syscall.h */
