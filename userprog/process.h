#ifndef USERPROG_PROCESS_H
#define USERPROG_PROCESS_H

#include "threads/thread.h"
#include <list.h>
#include "userprog/syscall.h"

/* This is to record the mmap of the current thread. */
struct mmap_container
  {
    int fd;
    mapid_t mapping_ID;
    struct page_entry * page_entry;
    struct list_elem elem;
  };

/* This is to record the children of the current thread. */
struct child_process_container
  {
    int tid;
    struct semaphore wait_sema;
    int child_ret_status;
    struct list_elem elem;
  };

tid_t process_execute (const char *file_name);
int process_wait (tid_t);
void process_exit (void);
void process_activate (void);

bool
install_page (void *upage, void *kpage, bool writable);

#endif /* userprog/process.h */
