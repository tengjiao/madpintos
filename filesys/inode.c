#include "filesys/inode.h"
#include <list.h>
#include <debug.h>
#include <round.h>
#include <string.h>
#include "threads/thread.h"
#include "filesys/filesys.h"
#include "filesys/free-map.h"
#include "filesys/cache.h"
#include "threads/malloc.h"

/* Identifies an inode. */
#define INODE_MAGIC 0x494e4f44
#define SINGLE_POINTER 3
#define DOUBLE_POINTER 120

/* On-disk inode.
 Must be exactly BLOCK_SECTOR_SIZE bytes long. */
struct inode_disk
{
  off_t length; /* File size in bytes. */
  block_sector_t single_p[SINGLE_POINTER]; /* Single level pointer. */
  block_sector_t double_p[DOUBLE_POINTER]; /* Double level pointer. */
  block_sector_t triple_p;
  unsigned magic; /* Magic number. */
  bool isdir; /* is directory */
  block_sector_t parent_dir; /* parent directory */
};

struct indexed_block
{
  block_sector_t sector[128];
};

/* Returns the number of sectors to allocate for an inode SIZE
 bytes long. */
static inline size_t
bytes_to_sectors (off_t size)
{
  // e.g.
  // DIV_ROUND_UP(9,2)=5 i.e. floor(9/2)
  // DIV_ROUND_UP(10,2)=5 i.e. floor(10/2)
  // DIV_ROUND_UP(11,2)=5 i.e. floor(11/2)
  return DIV_ROUND_UP(size, BLOCK_SECTOR_SIZE);
}

/* In-memory inode. */
struct inode
{
  struct list_elem elem; /* Element in inode list. */
  block_sector_t sector; /* Sector number of disk location. */
  int open_cnt; /* Number of openers. */
  bool removed; /* True if deleted, false otherwise. */
  int deny_write_cnt; /* 0: writes ok, >0: deny writes. */
  bool isdir; /* is a directory */
  block_sector_t parent_block_sector; /* parent directory */

  int reader_cnt;
  int writer_cnt;
  struct lock lock;
  struct semaphore sema_reader;
  struct semaphore sema_writer;
  int num_waiting_reader;
};

/* Given an inode and position.
 Return the sector. */
static block_sector_t
find_sector_by_pos (const struct inode_disk *inode_disk, off_t pos)
{
  int virtual_sector = pos / BLOCK_SECTOR_SIZE;

  if (virtual_sector < SINGLE_POINTER)
    return inode_disk->single_p[virtual_sector];
  else if (virtual_sector < SINGLE_POINTER + DOUBLE_POINTER * 128)
    {
      int double_index = (virtual_sector - SINGLE_POINTER) / 128;
      int double_offset = (virtual_sector - SINGLE_POINTER) % 128;

      // allocate that double index and read it
      struct indexed_block *indexed_block = calloc (1, sizeof *indexed_block);
      ASSERT(indexed_block != NULL);

      // read it from disk
//      block_read (fs_device, inode_disk->double_p[double_index], indexed_block);

      struct cache * cache = get_cache_by_sector (
	  inode_disk->double_p[double_index], READ);

      if (cache == NULL)
	PANIC("cannot get cache to read!!");

      // read it from cache
//      struct cache *cache = get_cache_by_sector(inode_disk->double_p[double_index], READ);

      lock_acquire (&cache->cache_lock);
      memcpy (indexed_block, (uint8_t *) &cache->data, BLOCK_SECTOR_SIZE);
      block_sector_t sector_idx = indexed_block->sector[double_offset];
      cache->reader_cnt--;
      lock_release (&cache->cache_lock);

      free (indexed_block);

      return sector_idx;
    }

  NOT_REACHED()
  ;
  return NULL;
}

/* Given an inode and position. Grow the file to the pos.
 Return the last sector. */
static block_sector_t
grow_file_to_pos (struct inode *inode, struct inode_disk *inode_disk, off_t pos,
		  off_t size)
{
  int goal_sector = (pos + size) / BLOCK_SECTOR_SIZE;
  int current_length = inode_disk->length;
  int current_sector = inode_disk->length / BLOCK_SECTOR_SIZE;
  // strategy
  // 1. mark the meta inode as invalid in cache
  // 2. initialize new block with 0

  // TJ For testing
//  if (inode_length (inode) % 1234 == 0  )
//    {
//      printf ("\n******IN grow_file_to_pos()*******\n");
//      printf ("\n******goal_sector %d*******\n", goal_sector);
//      printf ("\n******current_sector %d*******\n", current_sector);
//      printf ("\n******new size %d*******\n", pos+size);
//    }

  if (goal_sector == current_sector && goal_sector < SINGLE_POINTER)
    {
      if ((inode_disk->length % BLOCK_SECTOR_SIZE) == 0)
	{
	  // allocate a sector
	  free_map_allocate (1, &inode_disk->single_p[current_sector]);
	}

      inode_disk->length = pos + size;
      // write 0 from current_length to pos
      struct cache *cache = get_cache_by_sector (
	  inode_disk->single_p[current_sector], WRITE);

      if (cache == NULL)
	PANIC("cannot get cache to write!!");

      lock_acquire (&cache->cache_lock);

      int bytes_write = pos - current_length;

      memset ((uint8_t *) &cache->data + current_length % BLOCK_SECTOR_SIZE, 0,
	      bytes_write);
      cache->is_dirty = true;
      cache->writer_cnt--;
      lock_release (&cache->cache_lock);

//      block_write (fs_device, inode->sector, inode_disk);
      cache = get_cache_by_sector (inode->sector, WRITE);

      if (cache == NULL)
	PANIC("cannot get cache to write!!");

      lock_acquire (&cache->cache_lock);

      memcpy ((uint8_t *) &cache->data, inode_disk, sizeof *inode_disk);

      cache->is_dirty = true;
      cache->writer_cnt--;
      lock_release (&cache->cache_lock);
      return goal_sector;
    }

  // modify bit-map
  if (goal_sector < SINGLE_POINTER)
    {
      if ((inode_disk->length % BLOCK_SECTOR_SIZE) == 0)
	{
	  // allocate a sector
	  free_map_allocate (1, &inode_disk->single_p[current_sector]);
	}

      inode_disk->length = pos + size;
      // fill the first sector
      struct cache *cache = get_cache_by_sector (
	  inode_disk->single_p[current_sector], WRITE);

      if (cache == NULL)
	PANIC("cannot get cache to write!!");

      lock_acquire (&cache->cache_lock);

      int i = 0;

      int bytes_write = BLOCK_SECTOR_SIZE - current_length % BLOCK_SECTOR_SIZE;

      memset ((uint8_t *) &cache->data + current_length % BLOCK_SECTOR_SIZE, 0,
	      bytes_write);
      cache->is_dirty = true;
      cache->writer_cnt--;
      lock_release (&cache->cache_lock);

      current_sector++;

      //modify inode-disk and allocate data
      while (goal_sector - current_sector >= 0)
	{
	  // write the middle sectors
	  // create a new sector
	  free_map_allocate (1, &inode_disk->single_p[current_sector]);
	  struct cache *cache = get_cache_by_sector (
	      inode_disk->single_p[current_sector], WRITE);
	  if (cache == NULL)
	    PANIC("cannot get cache to write!!");

	  lock_acquire (&cache->cache_lock);

	  memset ((uint8_t *) &cache->data, 0, BLOCK_SECTOR_SIZE);
	  cache->is_dirty = true;
	  cache->writer_cnt--;
	  lock_release (&cache->cache_lock);

	  current_sector++;
	}

//      block_write (fs_device, inode->sector, inode_disk);
      cache = get_cache_by_sector (inode->sector, WRITE);

      if (cache == NULL)
	PANIC("cannot get cache to write!!");

      lock_acquire (&cache->cache_lock);

      memcpy ((uint8_t *) &cache->data, inode_disk, sizeof *inode_disk);

      cache->is_dirty = true;
      cache->writer_cnt--;
      lock_release (&cache->cache_lock);
      return goal_sector;
    }

  // the hardest
  if (goal_sector < SINGLE_POINTER + DOUBLE_POINTER * 128)
    {
      inode_disk->length = pos + size;

//      if (inode_length (inode) % 1234 == 0  )
//        {
//          printf ("\n#########IN Second level##########\n");
//          printf ("\n******current_length %d*******\n", current_length  );
//          printf ("\n******write off_set %d*******\n", current_length % BLOCK_SECTOR_SIZE);
//          printf ("\n******bytes_write %d*******\n", BLOCK_SECTOR_SIZE - current_length % BLOCK_SECTOR_SIZE);
//        }

      // fill the first sector
      if ((current_length % BLOCK_SECTOR_SIZE) != 0)
	{
	  struct cache *cache = get_cache_by_sector (
	      find_sector_by_pos (inode_disk, pos), WRITE);

	  if (cache == NULL)
	    PANIC("cannot get cache to write!!");

	  lock_acquire (&cache->cache_lock);

	  int bytes_write = BLOCK_SECTOR_SIZE
	      - current_length % BLOCK_SECTOR_SIZE;

	  memset ((uint8_t *) &cache->data + current_length % BLOCK_SECTOR_SIZE,
		  0, bytes_write);

	  cache->is_dirty = true;
	  cache->writer_cnt--;

	  lock_release (&cache->cache_lock);

	  current_sector++;

	  current_length += bytes_write;
	}

      // now current sector might be in SINGLE or DOUBLE
      //modify inode-disk and allocate data

      // fill the single ones
      int sectors = goal_sector - current_sector + 1;

      while (sectors > 0 && current_sector < SINGLE_POINTER)
	{
//	  if (inode_length (inode) % 1234 == 0  )
//	    {
//	      printf ("\n#########IN Second level SINGLE_POINTER##########\n");
//	      printf ("\n******sectors %d*******\n", sectors);
//	      printf ("\n******current_sector %d*******\n", current_sector);
//	    }

	  free_map_allocate (1, &inode_disk->single_p[current_sector]);
	  struct cache *cache = get_cache_by_sector (
	      inode_disk->single_p[current_sector], WRITE);

	  if (cache == NULL)
	    PANIC("cannot get cache to write!!");

	  lock_acquire (&cache->cache_lock);

	  memset ((uint8_t *) &cache->data, 0, BLOCK_SECTOR_SIZE);

	  cache->is_dirty = true;
	  cache->writer_cnt--;

	  lock_release (&cache->cache_lock);

	  current_sector++;
	  sectors--;
	  if (current_sector >= SINGLE_POINTER)
	    break;
	}

      // fill the double ones
      while (sectors > 0
	  && current_sector < SINGLE_POINTER + DOUBLE_POINTER * 128)
	{
//	  if ( inode_disk->length == 76543  )
//	    {
//	      printf ("\n******sectors %d*******\n", sectors);
//	      printf ("\n******current_sector %d*******\n", current_sector);
//	    }

	  struct indexed_block *indexed_block = calloc (1,
							sizeof *indexed_block);

	  int sectors_allocated = 0;

	  if (sectors > 128)
	    sectors_allocated = 128;
	  else
	    sectors_allocated = sectors;

//	  if (inode_length (inode) % 1234 == 0  )
//	    {
//	      printf ("\n******sectors_allocated %d*******\n", sectors_allocated);
//	    }

	  // if this index is 0, that means this is a new inode_disk->double_p
	  int second_sectors = current_length / BLOCK_SECTOR_SIZE
	      - SINGLE_POINTER;
	  int current_offset = second_sectors % 128;
	  int current_index = second_sectors / 128;

//	  if (inode_length (inode) % 1234 == 0  )
//	    {
//	      printf ("\n******current_length %d*******\n", current_length);
//	      printf ("\n******second_sectors %d*******\n", second_sectors);
//	    }
	  if (second_sectors < 0)
	    {
	      current_offset = 0;
	      current_index = 0;
	    }

	  if (sectors_allocated > 128 - current_offset)
	    sectors_allocated = 128 - current_offset;

//	  if (inode_length (inode) % 1234 == 0  )
//	    {
//	      printf ("\n******current_index %d*******\n", current_index);
//	      printf ("\n******current_offset %d*******\n", current_offset);
//	    }

	  int i = current_offset;

	  // write the data into current indexed_block
	  if (current_length % BLOCK_SECTOR_SIZE != 0 && second_sectors >= 0)
	    {
	      i++;
	    }

	  // If the sector already exists, write the previous data
	  if ((current_sector - SINGLE_POINTER) % 128 != 0)
	    {
	      //block_sector_t sector= inode_disk->double_p [current_offset];
	      block_sector_t sector = inode_disk->double_p[(current_sector
		  - SINGLE_POINTER) / 128];
	      struct cache *cache = get_cache_by_sector (sector, WRITE);

	      if (cache == NULL)
		PANIC("cannot get cache to write!!");

	      lock_acquire (&cache->cache_lock);

	      memcpy (indexed_block, (uint8_t *) &cache->data,
		      BLOCK_SECTOR_SIZE);

	      cache->is_dirty = true;
	      cache->writer_cnt--;

	      lock_release (&cache->cache_lock);
	    }

	  // write the exact content on each sector of indexed_block
	  for (; i < current_offset + sectors_allocated; i++)
	    {
	      free_map_allocate (1, &indexed_block->sector[i]);

//	      if (inode_length (inode) % 1234 == 0  )
//		{
//		  printf ("\n******indexed_block->sector[%d] :  %d*******\n",i, indexed_block->sector[i]);
//		}

	      struct cache *cache = get_cache_by_sector (
		  indexed_block->sector[i], WRITE);

	      if (cache == NULL)
		PANIC("cannot get cache to write!!");

	      lock_acquire (&cache->cache_lock);

	      memset ((uint8_t *) &cache->data, 0, BLOCK_SECTOR_SIZE);

	      cache->is_dirty = true;
	      cache->writer_cnt--;
	      lock_release (&cache->cache_lock);
	    }

//	  if (inode_disk->length == 76543)
//	    {
//	      int i = 0;
//
//	      for (; i < 128; i++)
//		printf ("\n******indexed_block->sector[%d] :  %d*******\n", i,
//			indexed_block->sector[i]);
//	    }

	  // write the double_block to disk_inode
	  if (second_sectors < 0
	      || (current_length % BLOCK_SECTOR_SIZE == 0 && current_offset == 0))
	    {
	      free_map_allocate (
		  1,
		  &inode_disk->double_p[(current_sector - SINGLE_POINTER) / 128]);
	    }
	  if (inode_disk->double_p[(current_sector - SINGLE_POINTER) / 128]
	      == NULL)
	    free_map_allocate (
		1,
		&inode_disk->double_p[(current_sector - SINGLE_POINTER) / 128]);

//          block_write (fs_device, inode_disk->double_p [(current_sector-SINGLE_POINTER)/128], indexed_block);

	  struct cache *cache = get_cache_by_sector (
	      inode_disk->double_p[(current_sector - SINGLE_POINTER) / 128],
	      WRITE);

//printf("call5: %i\n", inode_disk->double_p [(current_sector-SINGLE_POINTER)/128]);
	  if (cache == NULL)
	    PANIC("cannot get cache to write!!");

	  lock_acquire (&cache->cache_lock);

	  memcpy ((uint8_t *) &cache->data, indexed_block,
		  sizeof *indexed_block);

	  cache->is_dirty = true;
	  cache->writer_cnt--;
	  lock_release (&cache->cache_lock);

//	  if (inode_length (inode) % 1234 == 0  )
//	    {
//	      printf ("\n******write to inode_disk->double_p[%d]*******\n", (current_sector-SINGLE_POINTER)/128);
//	      printf ("\n******this double pointer sector number %d *******\n", inode_disk->double_p [(current_sector-SINGLE_POINTER)/128]);
//	    }

	  free (indexed_block);

	  current_length = current_sector * BLOCK_SECTOR_SIZE;
	  current_sector += sectors_allocated;
	  sectors = sectors - sectors_allocated;

//	  if ( inode_disk->length == 76543  )
//	    {
//	      printf ("\n#########IN Second level DOUBLE_POINTER END LOOP##########\n");
//	      printf ("\n******sectors %d*******\n", sectors);
//	      printf ("\n******current_sector %d*******\n", current_sector);
//	    }

	  if (current_sector >= SINGLE_POINTER + DOUBLE_POINTER * 128)
	    break;
	}

//      block_write (fs_device, inode->sector, inode_disk);
      struct cache *cache = get_cache_by_sector (inode->sector, WRITE);

      if (cache == NULL)
	PANIC("cannot get cache to write!!");

      lock_acquire (&cache->cache_lock);

      memcpy ((uint8_t *) &cache->data, inode_disk, sizeof *inode_disk);

      cache->is_dirty = true;
      cache->writer_cnt--;
      lock_release (&cache->cache_lock);
      return goal_sector;
    }

  NOT_REACHED()
  ;
  return NULL;
}

/* Returns the block device sector that contains byte offset POS
 within INODE.
 Returns -1 if INODE does not contain data for a byte at offset
 POS. */
static block_sector_t
byte_to_sector (const struct inode *inode, off_t size, off_t pos,
		enum access_type access_type)
{
  // This design will be replaced by cache.
  ASSERT(inode != NULL);

  struct inode_disk *disk_inode = malloc (sizeof(struct inode_disk));
  if (disk_inode != NULL)
    {
//      block_read (fs_device, inode->sector, disk_inode);
      struct cache * cache = get_cache_by_sector (inode->sector, READ);

      lock_acquire (&cache->cache_lock);
      memcpy (disk_inode, (uint8_t *) &cache->data, sizeof(struct inode_disk));
      cache->reader_cnt--;

      lock_release (&cache->cache_lock);
      block_sector_t sector_idx = -1;

      // TJ For testing
//      if(inode_length (inode) % 1234 == 0  )
//	{
//	  printf("\n******inode->sector %i*******\n", inode->sector);
//	  printf("\n******disk_inode->length %i %i*******\n",disk_inode->length, inode_length(inode));
      //printf("\n******pos %d*******\n",pos);
      //printf("\n******size %d*******\n",size);
////	  printf("\n******disk_inode->single[0] %d*******\n",disk_inode->single_p[0]);
////	  printf("\n******disk_inode->single[1] %d*******\n",disk_inode->single_p[1]);
//	}

      if (pos < disk_inode->length)
	sector_idx = find_sector_by_pos (disk_inode, pos);
      else if (pos >= disk_inode->length && access_type == WRITE)
	{
	  grow_file_to_pos (inode, disk_inode, pos, size);
	  sector_idx = find_sector_by_pos (disk_inode, pos);
	}
      else
	sector_idx = -1;

      free (disk_inode);
      return sector_idx;
    }

  NOT_REACHED()
  ;
  return NULL;
}

static void
free_all_sectors (struct inode_disk * inode_disk)
{
//  free_map_release (disk_inode->start, bytes_to_sectors (disk_inode->length));
  int length = inode_disk->length / BLOCK_SECTOR_SIZE;

  if (inode_disk->length % BLOCK_SECTOR_SIZE)
    {
      length++;
    }

  int single_index = 0;
  while (length > 0)
    {
      free_map_release (inode_disk->single_p[single_index], 1);
      single_index++;
      length--;

      if (single_index >= SINGLE_POINTER)
	break;
    }

  int double_num = 0; // < DOUBLE_POINTER

  while (length > 0)
    {
      struct indexed_block *indexed_block = calloc (1, sizeof *indexed_block);
      ASSERT(indexed_block != NULL);

      // read it from disk
//      block_read (fs_device, inode_disk->double_p[double_num], indexed_block);
      struct cache * cache = get_cache_by_sector (
	  inode_disk->double_p[double_num], READ);

      lock_acquire (&cache->cache_lock);
      memcpy (indexed_block, (uint8_t *) &cache->data, sizeof *indexed_block);
      cache->reader_cnt--;

      lock_release (&cache->cache_lock);

      int length_freed = 0;

      if (length > BLOCK_SECTOR_SIZE)
	length_freed = BLOCK_SECTOR_SIZE;
      else
	length_freed = length;

      int i = 0;

      for (i = 0; i < length_freed; i++)
	{
	  free_map_release (indexed_block->sector[i], 1);
	}

      free_map_release (inode_disk->double_p[double_num], 1);

      free (indexed_block);

      double_num++;
      length = length - length_freed;

      if (double_num >= DOUBLE_POINTER)
	break;
    }
}

/* List of open inodes, so that opening a single inode twice
 returns the same `struct inode'. */
static struct list open_inodes;

/* Initializes the inode module. */
void
inode_init (void)
{
  list_init (&open_inodes);
}

/* Initializes an inode with LENGTH bytes of data and
 writes the new inode to sector SECTOR on the file system
 device.
 Returns true if successful.
 Returns false if memory or disk allocation fails. */
bool
inode_create (block_sector_t sector, off_t length, bool isdir)
{
  struct inode_disk *disk_inode = NULL;
  bool success = false;

  ASSERT(length >= 0);

  /* If this assertion fails, the inode structure is not exactly
   one sector in size, and you should fix that. */
  ASSERT(sizeof *disk_inode == BLOCK_SECTOR_SIZE);

  disk_inode = calloc (1, sizeof *disk_inode);

  ASSERT(disk_inode!= NULL);

  if (disk_inode != NULL)
    {
      size_t sectors = bytes_to_sectors (length);
      disk_inode->length = length;
      disk_inode->magic = INODE_MAGIC;
      disk_inode->isdir = isdir;
      disk_inode->parent_dir = 0;

      // allocate the single pointers
      int single_num = 0; // < SINGLE_POINTER

      static char zeros[BLOCK_SECTOR_SIZE];
      while (sectors > 0)
	{
	  free_map_allocate (1, &disk_inode->single_p[single_num]);
	  //block_write (fs_device, disk_inode->single_p [single_num], zeros);
	  struct cache *cache = get_cache_by_sector (
	      disk_inode->single_p[single_num], WRITE);

	  if (cache == NULL)
	    PANIC("cannot get cache to write!!");

	  lock_acquire (&cache->cache_lock);

	  memset ((uint8_t *) &cache->data, 0, BLOCK_SECTOR_SIZE);

	  cache->is_dirty = true;
	  cache->writer_cnt--;
	  lock_release (&cache->cache_lock);

	  single_num++;
	  sectors--;
	  if (single_num >= SINGLE_POINTER)
	    break;
	}

      int double_num = 0; // < DOUBLE_POINTER

      // allocate the second level pointer
      while (sectors > 0)
	{
	  struct indexed_block *indexed_block = calloc (1,
							sizeof *indexed_block);
	  ASSERT(indexed_block != NULL);

	  int sectors_allocated = 0;

	  if (sectors > 128)
	    sectors_allocated = 128;
	  else
	    sectors_allocated = sectors;

	  int i = 0;

	  // write the exact content on each sector of indexed_block
	  for (i = 0; i < sectors_allocated; i++)
	    {
	      free_map_allocate (1, &indexed_block->sector[i]);
//              block_write (fs_device, indexed_block->sector[i], zeros);

	      struct cache *cache = get_cache_by_sector (
		  indexed_block->sector[i], WRITE);

	      if (cache == NULL)
		PANIC("cannot get cache to write!!");

	      lock_acquire (&cache->cache_lock);

	      memset ((uint8_t *) &cache->data, 0, BLOCK_SECTOR_SIZE);

	      cache->is_dirty = true;
	      cache->writer_cnt--;
	      lock_release (&cache->cache_lock);
	    }

	  // write the double_block to disk_inode
	  free_map_allocate (1, &disk_inode->double_p[double_num]);
//	  block_write (fs_device, disk_inode->double_p [double_num], indexed_block);
	  struct cache *cache = get_cache_by_sector (
	      disk_inode->double_p[double_num], WRITE);

	  if (cache == NULL)
	    PANIC("cannot get cache to write!!");

	  lock_acquire (&cache->cache_lock);

	  memcpy ((uint8_t *) &cache->data, indexed_block,
		  sizeof *indexed_block);

	  cache->is_dirty = true;
	  cache->writer_cnt--;
	  lock_release (&cache->cache_lock);

	  free (indexed_block);

	  double_num++;
	  sectors = sectors - sectors_allocated;

	  if (double_num >= DOUBLE_POINTER)
	    break;
	}

//      block_write (fs_device, sector, disk_inode);
      struct cache *cache = get_cache_by_sector (sector, WRITE);

      if (cache == NULL)
	PANIC("cannot get cache to write!!");

      lock_acquire (&cache->cache_lock);

      memcpy ((uint8_t *) &cache->data, disk_inode, sizeof *disk_inode);

      cache->is_dirty = true;
      cache->writer_cnt--;
      lock_release (&cache->cache_lock);
      free (disk_inode);
      success = true;
    }

  return success;
}

/* Reads an inode from SECTOR
 and returns a `struct inode' that contains it.
 Returns a null pointer if memory allocation fails. */
struct inode *
inode_open (block_sector_t sector)
{
  struct list_elem *e;
  struct inode *inode;

  /* Check whether this inode is already open. */
  for (e = list_begin (&open_inodes); e != list_end (&open_inodes); e =
      list_next (e))
    {
      inode = list_entry(e, struct inode, elem);
      if (inode->sector == sector)
	{
	  inode_reopen (inode);
	  return inode;
	}
    }

  /* Allocate memory. */
  inode = malloc (sizeof *inode);
  if (inode == NULL)
    return NULL;

  /* Initialize. */
  list_push_front (&open_inodes, &inode->elem);
  inode->sector = sector;
  inode->open_cnt = 1;
  inode->deny_write_cnt = 0;
  inode->removed = false;

  struct cache * cache = get_cache_by_sector (inode->sector, READ);

  lock_acquire (&cache->cache_lock);

  struct inode_disk *data = (struct inode_disk *) (cache->data);
  inode->isdir = data->isdir;
  inode->parent_block_sector = data->parent_dir;

  cache->reader_cnt--;

  lock_release (&cache->cache_lock);

  lock_init (&inode->lock);
  sema_init (&inode->sema_reader, 0);
  sema_init (&inode->sema_writer, 0);
  inode->reader_cnt = 0;
  inode->writer_cnt = 0;
  inode->num_waiting_reader = 0;

  // COMMENT OUT
//  block_read (fs_device, inode->sector, &inode->data);

  return inode;
}

/* Reopens and returns INODE. */
struct inode *
inode_reopen (struct inode *inode)
{
  if (inode != NULL)
    {
      lock_acquire (&inode->lock);
      inode->open_cnt++;
      lock_release (&inode->lock);
    }
  return inode;
}

/* Returns INODE's inode number. */
block_sector_t
inode_get_inumber (const struct inode *inode)
{
  return inode->sector;
}

/* Closes INODE and writes it to disk.
 If this was the last reference to INODE, frees its memory.
 If INODE was also a removed inode, frees its blocks. */
void
inode_close (struct inode *inode)
{
  /* Ignore null pointer. */
  if (inode == NULL)
    return;
  /* Release resources if this was the last opener. */
  lock_acquire (&inode->lock);
  if (--inode->open_cnt == 0)
    {
      /* Remove from inode list and release lock. */
      list_remove (&inode->elem);
      /* Deallocate blocks if removed. */
      if (inode->removed)
	{
	  // Find the meta-data and free data first
	  struct cache * cache = get_cache_by_sector (inode->sector, WRITE);

	  struct inode_disk* disk_inode = calloc (1, sizeof(struct inode_disk));

	  lock_acquire (&cache->cache_lock);

	  memcpy (disk_inode, (uint8_t *) &cache->data,
		  sizeof(struct inode_disk));

	  cache->is_valid = false;
	  cache->writer_cnt--;

	  lock_release (&cache->cache_lock);

	  if (disk_inode != NULL)
	    {
	      // Free the deeper level first
	      free_all_sectors (disk_inode);
	      free (disk_inode);

	    }
	  else
	    {
	      PANIC("cannot alloc disk_inode to free the bitmap!!");
	    }

	  free_map_release (inode->sector, 1);
	}
      lock_release (&inode->lock);

      free (inode);
      return;
    }
  lock_release (&inode->lock);
}

/* Marks INODE to be deleted when it is closed by the last caller who
 has it open. */
void
inode_remove (struct inode *inode)
{
  ASSERT(inode != NULL);
  lock_acquire (&inode->lock);
  inode->removed = true;
  lock_release (&inode->lock);
}

/* Reads SIZE bytes from INODE into BUFFER, starting at position OFFSET.
 Returns the number of bytes actually read, which may be less
 than SIZE if an error occurs or end of file is reached. */
off_t
inode_read_at (struct inode *inode, void *buffer_, off_t size, off_t offset)
{
  uint8_t *buffer = buffer_;
  off_t bytes_read = 0;
//  uint8_t *bounce = NULL;
  if (inode->writer_cnt > 0)
    {
      if (inode->reader_cnt == 0)
	{
	  sema_up (&inode->sema_writer);
	}
      lock_acquire (&inode->lock);
      inode->num_waiting_reader++;
      lock_release (&inode->lock);
      sema_down (&inode->sema_reader);
    }
  lock_acquire (&inode->lock);
  inode->reader_cnt++;
  lock_release (&inode->lock);

  while (size > 0)
    {
      /* Disk sector to read, starting byte offset within sector. */
      block_sector_t sector_idx = byte_to_sector (inode, size, offset, READ);
      int sector_ofs = offset % BLOCK_SECTOR_SIZE;

      /* Bytes left in inode, bytes left in sector, lesser of the two. */
      off_t inode_left = inode_length (inode) - offset;
      int sector_left = BLOCK_SECTOR_SIZE - sector_ofs;
      int min_left = inode_left < sector_left ? inode_left : sector_left;

      /* Number of bytes to actually copy out of this sector. */
      int chunk_size = size < min_left ? size : min_left;
      if (chunk_size <= 0)
	break;

//      printf("\n*******thread current: %s********\n", thread_current()->name);
//      printf("\n*******get cache to read********\n");
      bool is_wirte = false;
      struct cache *cache = get_cache_by_sector (sector_idx, READ);

      // ***************************************************************
      // This will be modified. For the indexed version, this might not
      // be the next sector_idx.
      //spawn_read_ahead_thread( sector_idx + 1 );

//      printf("\n******* cache->sector_id: %d ********\n",cache->sector_id);
//      printf("\n******* offset: %d ********\n",offset);
//      printf("\n******* size: %d ********\n",size);
//      printf("\n******* chunk_size: %d ********\n",chunk_size);
//      printf("\n*******get done********\n");

      if (cache == NULL)
	PANIC("cannot get cache to read!!");

      lock_acquire (&cache->cache_lock);
      memcpy (buffer + bytes_read, (uint8_t *) &cache->data + sector_ofs,
	      chunk_size);
//      printf("\n*******buffer: %x || cache->data: %x********\n",*(buffer+ bytes_read),cache->data[sector_ofs]);
//      printf("\n*******buffer: %x || cache->data: %x********\n",*(buffer+ bytes_read+5),cache->data[sector_ofs+5]);
      cache->reader_cnt--;
      lock_release (&cache->cache_lock);

      /* Advance. */
      size -= chunk_size;
      offset += chunk_size;
      bytes_read += chunk_size;

      block_sector_t sector_idx_next = byte_to_sector (inode, size, offset + chunk_size, READ);
      if(sector_idx_next <=1 && sector_idx_next >= 16384)
        spawn_read_ahead_thread (sector_idx_next);
    }
//  free (bounce);

//  printf("\n***XXXXXX read is done XXXXXX***\n");
  lock_acquire (&inode->lock);
  inode->reader_cnt--;
  if (inode->writer_cnt > 0)
    {
      sema_up (&inode->sema_writer);
    }
  else if (inode->num_waiting_reader > 0)
    {
      inode->num_waiting_reader++;
      sema_up (&inode->sema_reader);
    }
  lock_release (&inode->lock);

  return bytes_read;
}

/* Writes SIZE bytes from BUFFER into INODE, starting at OFFSET.
 Returns the number of bytes actually written, which may be
 less than SIZE if end of file is reached or an error occurs.
 (Normally a write at end of file would extend the inode, but
 growth is not yet implemented.) */
off_t
inode_write_at (struct inode *inode, const void *buffer_, off_t size,
		off_t offset)
{
  const uint8_t *buffer = buffer_;
  off_t bytes_written = 0;
//  uint8_t *bounce = NULL;

  // TJ For testing
//  if(inode_length (inode) % 1234 == 0 )
//    {
//      printf("\n*************inode_write_at call************\n");
//      printf("\n**********inode address %x***************\n",inode);
//      printf("\n**********buffer %c***************\n",*buffer);
//      printf("\n**********size %d***************\n",size);
//      printf("\n**********offset %d***************\n",offset);
//    }

  if (inode->deny_write_cnt)
    {
      return 0;
    }

//printf("inode_write_at acquiring lock:%i inode:%i\n", thread_tid(), inode->sector);
  lock_acquire (&inode->lock);
  inode->writer_cnt++;
  if (inode->reader_cnt > 0)
    {
      lock_release (&inode->lock);
      sema_down (&inode->sema_writer);
      lock_acquire (&inode->lock);
    }
//printf("inode_write_at acquired lock:%i\n", thread_tid());
  while (size > 0)
    {
      /* Sector to write, starting byte offset within sector. */
      block_sector_t sector_idx = byte_to_sector (inode, size, offset, WRITE);
      int sector_ofs = offset % BLOCK_SECTOR_SIZE;

      //  TJ For testing
//      if(inode_length (inode) % 1234 == 0 )
//        {
//          printf("\n**********sector_idx %d***************\n",sector_idx);
//        }

      /* Bytes left in inode, bytes left in sector, lesser of the two. */
      off_t inode_left = inode_length (inode) - offset;

      //  TJ For testing
//      if(inode_length (inode) % 1234 == 0 )
//        {
//          printf("\n**********inode_left %d***************\n",inode_left);
//        }

      int sector_left = BLOCK_SECTOR_SIZE - sector_ofs;
      int min_left = inode_left < sector_left ? inode_left : sector_left;

      /* Number of bytes to actually write into this sector. */
      int chunk_size = size < min_left ? size : min_left;

      //  TJ For testing
//      if(inode_length (inode) % 1234 == 0 )
//        {
//          printf("\n**********chunk_size %d***************\n",chunk_size);
//        }

      if (chunk_size <= 0)
	break;

//      printf("\n*******thread current: %s********\n", thread_current()->name);
//      printf("\n*******get cache to write********\n");
      struct cache *cache = get_cache_by_sector (sector_idx, WRITE);

//      printf("\n******* cache->sector_id: %d ********\n",cache->sector_id);
//      printf("\n******* offset: %d ********\n",offset);
//      printf("\n******* size: %d ********\n",size);
//      printf("\n******* chunk_size: %d ********\n",chunk_size);
//      printf("\n*******get done********\n");

      if (cache == NULL)
	PANIC("cannot get cache to write!!");

      lock_acquire (&cache->cache_lock);
      memcpy ((uint8_t *) &cache->data + sector_ofs, buffer + bytes_written,
	      chunk_size);
//      printf("\n*******buffer: %x || cache->data: %x********\n",*(buffer+ bytes_written),cache->data[sector_ofs]);
//      printf("\n*******buffer: %x || cache->data: %x********\n",*(buffer+ bytes_written+5),cache->data[sector_ofs+5]);
      cache->is_dirty = true;
      cache->writer_cnt--;
      lock_release (&cache->cache_lock);

      /* Advance. */
      size -= chunk_size;
      offset += chunk_size;
      bytes_written += chunk_size;
    }

  inode->writer_cnt--;
  if (inode->num_waiting_reader > 0)
    {
      inode->num_waiting_reader--;
      sema_up (&inode->sema_reader);
    }
  else if (inode->writer_cnt > 0)
    {
      sema_up (&inode->sema_writer);
    }
//printf("inode_write_at releasing lock:%i\n", thread_tid());
  lock_release (&inode->lock);
//printf("inode_write_at released lock:%i\n", thread_tid());

//  printf("\n***XXXXXX write is done XXXXXX***\n");
  return bytes_written;
}

/* Disables writes to INODE.
 May be called at most once per inode opener. */
void
inode_deny_write (struct inode *inode)
{
  inode->deny_write_cnt++;
  ASSERT(inode->deny_write_cnt <= inode->open_cnt);
}

/* Re-enables writes to INODE.
 Must be called once by each inode opener who has called
 inode_deny_write() on the inode, before closing the inode. */
void
inode_allow_write (struct inode *inode)
{
  ASSERT(inode->deny_write_cnt > 0);
  ASSERT(inode->deny_write_cnt <= inode->open_cnt);
  inode->deny_write_cnt--;
}

/* Returns the length, in bytes, of INODE's data. */
off_t
inode_length (const struct inode *inode)
{
//  return inode->length;
//    // This design will be replaced by cache.

//
////    lock_acquire(&cache->cache_lock);
////
////
////
////    lock_release(&cache->cache_lock);
//
  struct cache * cache = get_cache_by_sector (inode->sector, READ);
  struct inode_disk *disk_inode = NULL;
  disk_inode = calloc (1, sizeof *disk_inode);
  if (disk_inode != NULL)
    {
//      block_read (fs_device, inode->sector, disk_inode);

      lock_acquire (&cache->cache_lock);
      memcpy (disk_inode, cache->data, sizeof *disk_inode);
      cache->reader_cnt--;
      lock_release (&cache->cache_lock);

      off_t length = -1;

      length = disk_inode->length;

      free (disk_inode);

      return length;
    }

  NOT_REACHED()
  ;
  return NULL;
}

bool
inode_set_parent_dir (block_sector_t child, block_sector_t parent)
{
  struct inode* inode = inode_open (child);
  if (inode == NULL)
    return false;
  inode->parent_block_sector = parent;
  inode_close (inode);
  return true;
}
block_sector_t
inode_get_parent (struct inode* inode)
{
  if (inode == NULL)
    return 0;
  return inode->parent_block_sector;
}
bool
inode_isdir (struct inode* inode)
{
  if (inode == NULL)
    return false;
  return inode->isdir;
}
int
inode_opencnt (struct inode* inode)
{
  return inode->open_cnt;
}
