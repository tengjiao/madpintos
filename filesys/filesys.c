#include "filesys/filesys.h"
#include <debug.h>
#include <stdio.h>
#include <string.h>
#include "filesys/file.h"
#include "filesys/free-map.h"
#include "filesys/inode.h"
#include "filesys/directory.h"
#include "filesys/cache.h"
#include "threads/thread.h"

/* Partition that contains the file system. */
struct block *fs_device;

struct lock filesys_lock;

static void do_format (void);

/* Initializes the file system module.
 If FORMAT is true, reformats the file system. */
void
filesys_init (bool format)
{
  fs_device = block_get_role (BLOCK_FILESYS);
  if (fs_device == NULL)
    PANIC("No file system device found, can't initialize file system.");

  inode_init ();
  cache_init ();
  free_map_init ();

  if (format)
    do_format ();

  free_map_open ();
}

/* Shuts down the file system module, writing any unwritten data
   to disk. */
void
filesys_done (void)
{
  bool is_filesys_done = true;
  write_back_all_dirty_caches (is_filesys_done);
  free_map_close ();
}

/* Creates a file named NAME with the given INITIAL_SIZE.
   Returns true if successful, false otherwise.
   Fails if a file named NAME already exists,
   or if internal memory allocation fails. */
bool
filesys_create (const char *name, off_t initial_size, bool isdir)
{
  if (strlen (name) == 0)
    return false;
  block_sector_t inode_sector = 0;

  struct dir *dir = get_dir_from_name (name);
  char *file_name = get_file_name_from_name (name);
  bool success = false;
  if (strcmp (file_name, ".") != 0 && strcmp (file_name, "..") != 0)
    {
      success = (dir != NULL && free_map_allocate (1, &inode_sector)
	  && inode_create (inode_sector, initial_size, isdir)
	  && dir_add (dir, file_name, inode_sector));
      if (!success && inode_sector != 0)
	free_map_release (inode_sector, 1);
      /* add parent dir */
      if (dir != NULL && isdir)
	{
	  inode_set_parent_dir (inode_sector,
				inode_get_inumber (dir_get_inode (dir)));
	}
    }
  dir_close (dir);
  free (file_name);
  return success;
}

/* Opens the file with the given NAME.
 Returns the new file if successful or a null pointer
 otherwise.
 Fails if no file named NAME exists,
 or if an internal memory allocation fails. */
struct file *
filesys_open (const char *name)
{
  if (strlen (name) == 0)
    return NULL;
  struct dir *dir = get_dir_from_name (name);
  char *file_name = get_file_name_from_name (name);
  struct inode *inode = NULL;

  if (dir != NULL)
    {
      if (file_name == NULL || strcmp (file_name, ".") == 0)
	{
	  /* THIS IS ROOT DIR */
	  free (file_name);
	  struct file* file = (struct file*) dir;
	  file->deny_write = false;
	  return file;
	}
      else if (strcmp (file_name, "..") == 0)
	{
	  inode = inode_open (inode_get_parent (dir_get_inode (dir)));
	}
      else
	{
	  dir_lookup (dir, file_name, &inode);
	}
    }

  dir_close (dir);
  free (file_name);

  return file_open (inode);
}

/* Deletes the file named NAME.
 Returns true if successful, false on failure.
 Fails if no file named NAME exists,
 or if an internal memory allocation fails. */
bool
filesys_remove (const char *name)
{

  if (strlen (name) == 0)
    return false;
  struct dir *dir = get_dir_from_name (name);
  char *file_name = get_file_name_from_name (name);
  if (file_name == NULL)
    {
      /* ROOT DIRECTORY */
      dir_close (dir);
      free (file_name);
      return false;
    }
  bool success = dir != NULL && dir_remove (dir, file_name);
  dir_close (dir);
  free (file_name);
  return success;
}

/* Formats the file system. */
static void
do_format (void)
{
  printf ("Formatting file system...");
  free_map_create ();
  if (!dir_create (ROOT_DIR_SECTOR, 16))
    PANIC("root directory creation failed");
  free_map_close ();
  printf ("done.\n");
}
struct dir *
get_dir_from_name (const char * name)
{
  if (strlen (name) == 0)
    return NULL;

  struct dir* goal_dir = NULL;
  struct inode* goal_inode = NULL;

  char *tmp3;
  char tmp2[strlen (name) + 1];
  memcpy (tmp2, name, strlen (name) + 1);

  if (tmp2[0] == '/')
    {
      goal_dir = dir_open_root ();
    }
  else
    {
      goal_dir = dir_reopen (thread_current ()->current_dir);
    }

  goal_inode = dir_get_inode (goal_dir);

  char* tmp = strtok_r (tmp2, "/", &tmp3);
  char* tmp_next = strtok_r (NULL, "/", &tmp3);

  while (tmp_next != NULL)
    {
      if (strcmp (tmp, ".") == 0)
	{
	  /* Do nothing */
	  tmp = tmp_next;
	  tmp_next = strtok_r (NULL, "/", &tmp3);
	  continue;
	}
      else if (strcmp (tmp, "..") == 0)
	{
	  if (inode_get_parent (goal_inode) == 0)
	    {
	      return NULL;
	    }
	  goal_inode = inode_open (inode_get_parent (goal_inode));
	}
      else
	{
	  //bool dir_lookup (const struct dir *, const char *name, struct inode **);
	  if (!dir_lookup (goal_dir, tmp, &goal_inode))
	    {
	      return NULL;
	    }
	}
      if (inode_isdir (goal_inode))
	{
	  dir_close (goal_dir);
	  goal_dir = dir_open (goal_inode);
	}
      else
	{
	  inode_close (goal_inode);
	}

      tmp = tmp_next;
      tmp_next = strtok_r (NULL, "/", &tmp3);
    }
  return goal_dir;
}
char *
get_file_name_from_name (const char *name)
{
  if (strlen (name) == 0)
    return name;
  char *tmp3;
  char tmp2[strlen (name) + 1];
  memcpy (tmp2, name, strlen (name) + 1);

  char* tmp = strtok_r (tmp2, "/", &tmp3);
  char* tmp_next = strtok_r (NULL, "/", &tmp3);

  if (tmp == NULL)
    return NULL;

  while (tmp_next != NULL)
    {
      tmp = tmp_next;
      tmp_next = strtok_r (NULL, "/", &tmp3);
    }

  char * file_name = malloc (sizeof(char) * strlen (tmp) + 1);
  strlcpy (file_name, tmp, sizeof(char) * strlen (tmp) + 1);
  return file_name;
}
