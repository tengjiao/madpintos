#include "filesys/cache.h"
#include <debug.h>
#include "threads/thread.h"
#include "threads/malloc.h"
#include "filesys/filesys.h"
#include "devices/timer.h"

struct list cache_list;

static struct cache *
find_cache_by_sectorID (block_sector_t sector_id)
{
  struct list_elem * e;
  struct cache * cache;

  lock_acquire (&cache_list_lock);

  for (e = list_begin (&cache_list); e != list_end (&cache_list); e =
      list_next (e))
    {
      cache = list_entry(e, struct cache, elem);
      if (cache->sector_id == sector_id && cache->is_valid)
	{
	  lock_release (&cache_list_lock);
	  return cache;
	}
    }

  lock_release (&cache_list_lock);
  return NULL;
}

/* Evict a cache write it back if dirty return its pointer.
 NOTE: When you call this method, you must hold the cache_lock. */
static struct cache *
cache_evict ()
{
  struct list_elem *e = list_front (&cache_list);
  while (true)
    {
      if (e == list_end (&cache_list))
	e = list_front (&cache_list);

      struct cache *cache = list_entry(e, struct cache, elem);

      if (cache->reader_cnt
	  > 0|| cache->writer_cnt > 0 ||cache->cache_lock.holder!=NULL)
	{
	  e = list_next (e);
	  continue;
	}

      if (cache->is_accessed || cache->sector_id == 0 || cache->sector_id == 2)
	{
	  cache->is_accessed = false;
	  e = list_next (e);
	  continue;
	}
      else
	{
	  if (cache->is_dirty && cache->is_valid)
	    block_write (fs_device, cache->sector_id, &cache->data);

//printf("cache evict:%i\n", cache->sector_id);
	  return cache;
	}

    }

  NOT_REACHED()
  ;
  return NULL;
}

/* The function executed by the writing back dirty cache thread. */
static void
flush_dirty_caches ()
{
  while (true)
    {
      thread_sleep (100);
      write_back_all_dirty_caches (false);
    }
}

/* Read the block one more ahead. */
static void
cache_read_ahead (block_sector_t sector_idx)
{
  // Cannot call this method, this will add the read number
//  get_cache_by_sector (*sector_idx, false);
//  printf("\n*******[cache_read_ahead] 1 getting ahead %d*******\n",sector_idx);
  struct cache * cache = find_cache_by_sectorID (sector_idx);
  if (cache != NULL)
    {
      return;
    }

//  printf("\n*******[cache_read_ahead] 2 lock_acquire %d*******\n",sector_idx);
  lock_acquire (&cache_list_lock);
  int size = list_size (&cache_list);

  if (size < CACHE_SIZE)
    {
      /* malloc a new one. */
      cache = calloc (1, sizeof *cache);
      lock_init (&cache->cache_lock);
      list_push_back (&cache_list, &cache->elem);
    }
  else
    {
      /* If(size == CACHE_SIZE), we have to evict one. */
      cache = cache_evict ();
    }

//  printf("\n*******[cache_read_ahead] 3 lock release %d*******\n",sector_idx);

  lock_acquire (&cache->cache_lock);
  cache->is_accessed = true;
  cache->is_dirty = false;
  cache->is_valid = true;
  cache->reader_cnt = 0;
  cache->writer_cnt = 0;
  cache->sector_id = sector_idx;

  block_read (fs_device, cache->sector_id, &cache->data);
  lock_release (&cache->cache_lock);
  lock_release (&cache_list_lock);

//  printf("\n*******[cache_read_ahead] 4 lock released %d*******\n",sector_idx);
//  printf("\n*******[cache_read_ahead] 5 getting ahead OK %d*******\n",sector_idx);
}

void
cache_init ()
{
  list_init (&cache_list);
  lock_init (&cache_list_lock);
  thread_create ("dirty_cache_write_back", PRI_DEFAULT, flush_dirty_caches,
		 NULL);
}

/* Find a cache in the list or evict one. */
struct cache *
get_cache_by_sector (block_sector_t sector_idx, enum access_type access_type)
{
  struct cache * cache = find_cache_by_sectorID (sector_idx);
  if (cache != NULL)
    {
      cache->is_accessed = true;

      if (access_type == WRITE)
	cache->writer_cnt++;
      else if (access_type == READ)
	cache->reader_cnt++;
      return cache;
    }

  lock_acquire (&cache_list_lock);
  int size = list_size (&cache_list);

  if (size < CACHE_SIZE)
    {
      /* malloc a new one. */
      cache = calloc (1, sizeof *cache);
      lock_init (&cache->cache_lock);
      list_push_back (&cache_list, &cache->elem);
    }
  else
    {
      /* If(size == CACHE_SIZE), we have to evict one. */
      cache = cache_evict ();
    }

  if (cache == NULL)
    PANIC("cache cannot be allocated!");

  /* When we first create a cache, we won't set access bit as true
   Only the second access could change the access bit. */
  cache->is_accessed = false;
  cache->is_dirty = false;
  cache->is_valid = true;
  cache->reader_cnt = 0;
  cache->writer_cnt = 0;
  cache->sector_id = sector_idx;

  if (access_type == WRITE)
    cache->writer_cnt++;
  else if (access_type == READ)
    cache->reader_cnt++;

  /* Read the data into cache data. */
  block_read (fs_device, cache->sector_id, &cache->data);
  lock_release (&cache_list_lock);

  return cache;
}

/* Constantly write back dirty caches. When the file system is done
 Everything will be written back. */
void
write_back_all_dirty_caches (bool is_file_system_done)
{
  struct list_elem * e;
  struct cache * cache;

//  printf("\n***1****thread current: %s wirte back*********\n",thread_current()->name);

  lock_acquire (&cache_list_lock);

//  printf("\n***2****thread current: %s wirte back*********\n",thread_current()->name);
  for (e = list_begin (&cache_list); e != list_end (&cache_list);)
    {
      cache = list_entry(e, struct cache, elem);

//      printf("\n***3****thread current: %s wirte back*********\n",thread_current()->name);
      if (cache->is_dirty && cache->is_valid)
	{
	  lock_acquire (&cache->cache_lock);
	  block_write (fs_device, cache->sector_id, &cache->data);
	  lock_release (&cache->cache_lock);
	}
      if (is_file_system_done)
	{

//          printf("\n***4****thread current: %s wirte back*********\n",thread_current()->name);
	  e = list_remove (&cache->elem);

	  free (cache);
	  continue;
	}

      e = list_next (e);
    }

  lock_release (&cache_list_lock);
//  printf("\n***XXXX thread current: %s wirte back DONE XXXX*****\n",thread_current()->name);
}

/* Spawn a thread to read the block one more ahead. */
void
spawn_read_ahead_thread (block_sector_t sector_idx)
{
  if (find_cache_by_sectorID (sector_idx) != NULL)
    return;
//
//  printf("\n*********sector_idx: %d***********\n",sector_idx);
//  block_sector_t * sector_id_ahead = malloc(sizeof(block_sector_t));
//  *sector_id_ahead = sector_idx;
//  printf ("\n*********sector_id_ahead***********\n");
  thread_create ("cache_read_ahead", PRI_DEFAULT, cache_read_ahead, sector_idx);
}

void
cache_flush (block_sector_t sector_id)
{
  struct list_elem * e;
  struct cache * cache;
  for (e = list_begin (&cache_list); e != list_end (&cache_list); e =
      list_next (e))
    {
      cache = list_entry(e, struct cache, elem);
      if (cache->sector_id == sector_id)
	{
	  e = list_remove (&cache->elem);
	  free (cache);
	  return;
	}
    }
}
