#ifndef FILESYS_CACHE_H
#define FILESYS_CACHE_H

#include <stdbool.h>
#include <list.h>
#include "threads/synch.h"
#include "devices/block.h"
#include "filesys/inode.h"

#define CACHE_SIZE 64

struct lock cache_list_lock;

struct cache
  {
    /* Disk_sector_id. */
    block_sector_t sector_id;
    /* Access bit. */
    bool is_accessed;
    /* Dirty bit. */
    bool is_dirty;
    /* Valid bit. */
    bool is_valid;
    /* # of readers. */
    int reader_cnt;
    /* # of writers. */
    int writer_cnt;

//  - # of pending read/write requests
//  - lock to protect above variables
    struct lock cache_lock;
//  - signaling variables to signal availability changes
//  - usage information for eviction policy

    /* Data (embedded). */
    uint8_t data[BLOCK_SECTOR_SIZE];

    struct list_elem elem;
  };

void cache_init ();
struct cache * get_cache_by_sector(block_sector_t , enum access_type);
void write_back_all_dirty_caches(bool);
void spawn_read_ahead_thread(block_sector_t sector_idx);

void cache_flush(block_sector_t);

#endif
