#ifndef VM_SUPP_PAGE
#define VM_SUPP_PAGE

#include <stdio.h>
#include <hash.h>
#include "filesys/file.h"

enum page_type
{
  ELF,              /* Page containing executable. */
  MMAP,             /* Page containing file(For MMAP). */
  STACK             /* Page containing STACK. */
};

struct page_entry
{
  /* Status record. */
  void * virtual_addr;
  bool is_writable;                /* Read-only page can be evict without swap. */
  bool is_loaded;                  /* Whether it is mapped to the PM. */
  bool is_being_mapped;            /* If a page is being mapped, page faults can be
                                      ignored. */

  enum page_type type;

  /* For files. */
  struct file* file;
  off_t ofs;
  uint32_t read_bytes;
  uint32_t zero_bytes;

  /* For swap. */
  bool is_swapout;                 /* For swap page, it is false. */
  size_t start_sector_in_swap;

  /* For page_entry_container mapping. */
  struct hash_elem elem;
};

void page_init();
void page_destroy();

/* Load the page entry. */
bool page_entry_load (struct file *file, off_t ofs, uint8_t *upage,
                 uint32_t read_bytes, uint32_t zero_bytes, bool writable,
                 enum page_type type);

/* Map page to the frame. */
bool page_install_frame(struct page_entry *);

/* Simply create an entry in the page table
 * and lazy install the page in the page
 * fault handler*/
bool supp_page_increase_stack (void *);

/* A useful tool
   --Given a virtual address
   --Return the page entry if found in page entry container
   --Return NULL if not found */
struct page_entry * find_page_by_address (const void *virtual_addr);

bool free_page_by_address (const void *virtual_addr);

#endif
