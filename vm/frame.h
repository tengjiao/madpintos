#ifndef VM_FRAME
#define VM_FRAME

#include <stdio.h>
#include <list.h>
#include "threads/thread.h"
#include "threads/palloc.h"
#include "vm/page.h"
#include "filesys/file.h"

struct list frame_list;

/* For synchronization
   e.g. Modification of frame_list by two threads. */
struct lock frame_lock;

struct frame
{
  /* Status record. */
  void * kernel_addr;
  struct page_entry * page_entry;
  struct thread * frame_holder;

  /* File attributes for sharing
     e.g. If they are same as some page_entry,
     we can simply load this frame. */
  struct file* file;
  off_t ofs;
  uint32_t read_bytes;
  uint32_t zero_bytes;

  /* For frame_list mapping. */
  struct list_elem elem;
};

void * frame_alloc (enum palloc_flags flags);
void * frame_create (enum palloc_flags , struct page_entry * );
void frame_destroy (struct frame *);
void frame_init();
struct frame* find_frame_by_kernel_addr(void*);
struct frame* find_frame_by_file (struct page_entry *);

#endif
