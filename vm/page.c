#include "vm/page.h"
#include <stdio.h>
#include <stdlib.h>
#include <bitmap.h>
#include <inttypes.h>
#include "threads/vaddr.h"
#include "threads/thread.h"
#include "threads/malloc.h"
#include "threads/palloc.h"
#include "userprog/process.h"
#include "vm/frame.h"
#include "vm/swap.h"

bool page_swap_back (struct page_entry * page_entry);
bool page_install_file (struct page_entry * page_entry);

/* Hash function used to initialize the page entry
   hash table. */
unsigned
page_hash_func (const struct hash_elem *e, void *aux)
{
  return hash_int ((int) hash_entry(e, struct page_entry, elem)->virtual_addr);
}

/* Hash Compare function used to initialize the page entry
   hash table. */
bool
page_hash_less_func (const struct hash_elem *a, const struct hash_elem *b, void *aux)
{
  return hash_entry(a, struct page_entry, elem)->virtual_addr -
  hash_entry(b, struct page_entry, elem)->virtual_addr;
}

/* Hash Free function used to destroy all the elements
   in the hash table. */
void
page_hash_free_func (struct hash_elem *e, void *aux)
{
  /* Free everything. */
  struct page_entry* page_entry;
  page_entry = hash_entry(e, struct page_entry, elem);

  if (page_entry->is_swapout)
    {
      swap_remove (page_entry->start_sector_in_swap);
    }

  void *kernel_addr = pagedir_get_page (thread_current ()->pagedir,
                                        page_entry->virtual_addr);
  if (kernel_addr != NULL)
    {
      struct frame *frame = find_frame_by_kernel_addr (kernel_addr);
      if (frame->frame_holder == thread_current ())
        {
         frame_destroy (frame);
        }
    }

  free (page_entry);
}

void
page_init()
{
  hash_init(&thread_current()->page_entry_container,page_hash_func,page_hash_less_func,NULL);
}

void
page_destroy()
{
  hash_destroy(&thread_current()->page_entry_container,page_hash_free_func);
}

/* Add an empty page entry and initialize the page
   entry with the file information. */
bool
page_entry_load(struct file *file, off_t ofs, uint8_t *upage,
                     uint32_t read_bytes, uint32_t zero_bytes, bool writable, enum page_type type)
{
  /* If this address is already mapped, we must return false. */
  if(find_page_by_address (upage))
    return false;

  struct page_entry * page_entry = malloc(sizeof(struct page_entry));

  page_entry->virtual_addr = upage;
  page_entry->is_writable = writable;
  page_entry->is_loaded = false;
  page_entry->is_being_mapped = false;
  page_entry->is_swapout = false;
  page_entry->type = type;
  page_entry->start_sector_in_swap = BITMAP_ERROR;

  page_entry->file = file;
  page_entry->ofs = ofs;
  page_entry->read_bytes = read_bytes;
  page_entry->zero_bytes = zero_bytes;

  return hash_insert(&thread_current()->page_entry_container,&page_entry->elem) == NULL;
}

/* Map the empty page entry to a physical frame. */
bool
page_install_frame (struct page_entry * page_entry)
{
  if (page_entry->is_swapout)
    return page_swap_back(page_entry);
  else if(page_entry->file != NULL)
    return page_install_file(page_entry);
  else
    return false;
}

/* Swap back the page in the swap disk. */
bool
page_swap_back (struct page_entry * page_entry)
{
  enum palloc_flags flags = PAL_USER;

  if (page_entry->zero_bytes)
    flags = flags | PAL_ZERO;

  struct frame* frame = frame_create (flags, page_entry);
  if (frame == NULL)
    {
      PANIC("cannot create frame!\n");
      return false;
    }

  /* swap in */
  if (!swap_in (frame->kernel_addr, page_entry->start_sector_in_swap))
    {
      PANIC("swap in failed!\n");
      return false;
    }

  if (!install_page (page_entry->virtual_addr, frame->kernel_addr,
                     page_entry->is_writable))
    {
      PANIC("swap in install page failed!\n");
      frame_destroy (frame);
      return false;
    }

  page_entry->is_loaded = true;
  return true;
}

/* Map the page entry with a file to a physical frame.
   This file could be an ELF file or a MMAP file. */
bool
page_install_file (struct page_entry * page_entry)
{
  if (page_entry == NULL)
    return false;

  /* If the file is read-only, we can find it on frame list
     without creating it. */
  if( page_entry->is_writable == false )
    {
      struct frame * frame = find_frame_by_file (page_entry);

      if (frame != NULL)
        {
         frame->page_entry = page_entry;
         frame->frame_holder = thread_current ();

         if (!install_page (page_entry->virtual_addr, frame->kernel_addr,
                             page_entry->is_writable))
            {
              PANIC("failed to install page\n");
              frame_destroy (frame);
              return false;
            }

          page_entry->is_loaded = true;
          return true;
        }
    }

  /* If the frame is not created yet, or the page is writable,
     then we need to create a frame to map that page. */
  enum palloc_flags flags = PAL_USER;
  if (page_entry->zero_bytes)
    flags = flags | PAL_ZERO;

  /* Get a page of memory. */
  struct frame * frame = frame_create (flags, page_entry);

  if (frame == NULL)
    return false;

  /* Load this page. */
  if (page_entry->read_bytes > 0)
    {
      lock_acquire(&syscall_lock);

      if (file_read_at (page_entry->file, frame->kernel_addr,
                        page_entry->read_bytes, page_entry->ofs)
         != (int) page_entry->read_bytes)
        {
          palloc_free_page (frame->kernel_addr);
          lock_release(&syscall_lock);
          return false;
        }

      lock_release (&syscall_lock);
      memset (frame->kernel_addr + page_entry->read_bytes, 0,
             page_entry->zero_bytes);
    }


  /* Add the page to the process's address space. */
  if (!install_page (page_entry->virtual_addr, frame->kernel_addr,
                    page_entry->is_writable))
    {
      PANIC("failed to install page\n");
      frame_destroy (frame);
      return false;
    }

  page_entry->is_loaded = true;
  return true;
}


/* Simply create an entry in the page table
   and lazy install the page in the page
   fault handler. */
bool
supp_page_increase_stack (void * vaddr)
{
  if (vaddr == NULL)
    return false; /* self-protection */

  struct page_entry *page_entry = malloc (sizeof(struct page_entry));
  page_entry->is_being_mapped = true;

  if (page_entry == NULL)
    return false;

  page_entry->virtual_addr = pg_round_down (vaddr);
  page_entry->file = NULL;
  page_entry->is_writable = true;
  page_entry->is_loaded = false;
  page_entry->is_swapout = false;
  page_entry->type = STACK;
  page_entry->start_sector_in_swap = BITMAP_ERROR;

  hash_insert (&thread_current ()->page_entry_container, &page_entry->elem);

  enum palloc_flags flags = PAL_USER;

  /* Get a page of memory. */
  struct frame * frame = frame_create (flags, page_entry);

  if (frame == NULL)
    {
      PANIC("cannot alloc frame\n");
      return false;
    }

  /* Add the page to the process's address space. */
  if (!install_page (page_entry->virtual_addr, frame->kernel_addr,
                    page_entry->is_writable))
    {
      PANIC("failed to install page\n");
      frame_destroy (frame);
      return false;
    }

  page_entry->is_loaded = true;
  page_entry->is_being_mapped = false;
  return true;
}

/* A useful tool
   --Given a virtual address
   --Return the page entry if found in page entry container
   --Return NULL if not found */
struct page_entry *
find_page_by_address (const void *virtual_addr)
{
  struct page_entry _page_entry;
  _page_entry.virtual_addr = pg_round_down (virtual_addr);

  struct hash_elem *e = hash_find (&thread_current ()->page_entry_container,
                                  &_page_entry.elem);

  if (e != NULL)
    return hash_entry(e, struct page_entry, elem);
  else
    return NULL;
}

/* A useful tool
   --Given a virtual address
   --Return true if page is freed
   --Return false if page is not freed */
bool
free_page_by_address (const void *virtual_addr)
{
  struct page_entry * page_entry = find_page_by_address (virtual_addr);

  if (page_entry == NULL)
    return false;

  struct hash_elem *deleted_elem = hash_delete (
      &thread_current ()->page_entry_container, &page_entry->elem);

  if (deleted_elem == NULL)
    return false;

  void *kernel_addr = pagedir_get_page (thread_current ()->pagedir,
                                        page_entry->virtual_addr);
  if (kernel_addr != NULL)
    {
      struct frame *frame = find_frame_by_kernel_addr (kernel_addr);
      if (frame->frame_holder == thread_current ())
        {
         frame_destroy (frame);
        }
    }

  free (page_entry);
  return true;
}
