#include "vm/frame.h"
#include <random.h>
#include "userprog/syscall.h"
#include "vm/swap.h"
#include "threads/palloc.h"

void *
frame_alloc (enum palloc_flags flags)
{
  lock_acquire (&frame_lock);

  void * kernel_addr = palloc_get_page (flags);
  if (kernel_addr != NULL)
    {
      lock_release(&frame_lock);
      return kernel_addr;
    }

  /* "kernel_addr == NULL" means NO FRAME AVAILABLE */

  /* The following codes make a frame eviction
     using circular algorithm. */
  struct list_elem *e = list_front (&frame_list);
  while (true)
    {
      if (e == list_end (&frame_list))
        e = list_front (&frame_list);

      struct frame *frame = list_entry(e, struct frame, elem);

      if (frame->page_entry->is_being_mapped)
        {
          e = list_next (e);
          continue;
        }


      /* If it is accessed recently, set the access bit to false. */
      if (pagedir_is_accessed (frame->frame_holder->pagedir,
                               frame->page_entry->virtual_addr))
        {
          pagedir_set_accessed (frame->frame_holder->pagedir,
                                frame->page_entry->virtual_addr, false);
          e = list_next (e);
          continue;
        }

      /* If it is MMAP, we can write it back to the disk without
         writing to the swap disk. */
      if (frame->page_entry->type == MMAP
          && pagedir_is_dirty (frame->frame_holder->pagedir,
                               frame->page_entry->virtual_addr))
        {
          if( syscall_lock.holder == thread_current() )
            {
              file_write_at (frame->page_entry->file,
                         frame->page_entry->virtual_addr,
                         frame->page_entry->read_bytes,
                         frame->page_entry->ofs);
            }
          else
            {
              lock_acquire (&syscall_lock);
              file_write_at (frame->page_entry->file,
                         frame->page_entry->virtual_addr,
                         frame->page_entry->read_bytes,
                         frame->page_entry->ofs);
              lock_release (&syscall_lock);
            }
        }
      /* If it is not MMAP, we can write it back to the swap disk. */
      else if (pagedir_is_dirty (frame->frame_holder->pagedir,
                                 frame->page_entry->virtual_addr))
        {
          frame->page_entry->is_swapout = true;
          swap_out (frame, &frame->page_entry->start_sector_in_swap);
        }

      frame->page_entry->is_loaded = false;

      pagedir_clear_page (frame->frame_holder->pagedir,
                          frame->page_entry->virtual_addr);
      palloc_free_page (frame->kernel_addr);
      list_remove (&frame->elem);
      free (frame);

      kernel_addr = palloc_get_page (flags);

      lock_release(&frame_lock);
      return kernel_addr;
    }

  NOT_REACHED();
  return NULL;
}

void
frame_init ()
{
  list_init (&frame_list);
  lock_init (&frame_lock);
}

/* Create a frame and map it to a page. */
void *
frame_create (enum palloc_flags flags, struct page_entry * page_entry)
{
  /* User bit is on the third bit. */
  int user_bit = (flags / 4) % 2;
  if (user_bit == 0)
    return NULL;

  struct frame * frame = malloc (sizeof(struct frame));

  if (frame == NULL)
    {
      return NULL;
    }

  frame->kernel_addr = frame_alloc (flags);

  if (frame->kernel_addr == NULL)
    {
      return NULL;
    }

  frame->page_entry = page_entry;
  frame->frame_holder = thread_current ();

  lock_acquire (&frame_lock);
  list_push_back (&frame_list, &frame->elem);
  lock_release (&frame_lock);

  /* Record the file info. */
  if (page_entry->type == ELF)
    {
      frame->file = page_entry->file;
      frame->ofs = page_entry->ofs;
      frame->read_bytes = page_entry->read_bytes;
      frame->zero_bytes = page_entry->zero_bytes;
    }

  return frame;
}

void
frame_destroy (struct frame * frame)
{
  lock_acquire (&frame_lock);
  pagedir_clear_page (frame->frame_holder->pagedir,
                      frame->page_entry->virtual_addr);
  palloc_free_page (frame->kernel_addr);
  list_remove (&frame->elem);
  free (frame);
  lock_release (&frame_lock);
}

struct frame*
find_frame_by_kernel_addr (void* kernel_addr)
{
  lock_acquire (&frame_lock);
  struct list_elem *e = list_front (&frame_list);
  struct frame *frame;
  while (e != list_tail (&frame_list))
    {
      frame = list_entry(e, struct frame, elem);
      if (frame->kernel_addr == kernel_addr)
        {
          lock_release (&frame_lock);
          return frame;
        }
      e = list_next(e);
    }

  lock_release (&frame_lock);
  return NULL;
}

struct frame*
find_frame_by_file (struct page_entry * page_entry)
{
  lock_acquire (&frame_lock);
  struct list_elem *e = list_front (&frame_list);
  struct frame *frame;
  while (e != list_tail (&frame_list))
    {
      frame = list_entry(e, struct frame, elem);
      if ( frame->file == page_entry->file &&
          frame->read_bytes == page_entry->read_bytes &&
          frame->zero_bytes == page_entry->zero_bytes &&
          frame->ofs == page_entry->ofs )
        {
          lock_release (&frame_lock);
          return frame;
        }
      e = list_next(e);
    }

  lock_release (&frame_lock);
  return NULL;
}
