#include "vm/swap.h"
#include <list.h>
#include <bitmap.h>
#include "threads/thread.h"
#include "threads/malloc.h"
#include "threads/synch.h"
#include "threads/palloc.h"

#define USED true
#define UNUSED false

static struct block *swap_block;
struct bitmap *bitmap;
struct lock swap_lock;

void
swap_init (void)
{
  lock_init (&swap_lock);
  swap_block = block_get_role (BLOCK_SWAP);
  if (swap_block != NULL)
    bitmap = bitmap_create ((size_t) block_size (swap_block) );

}

/* Swap out a frame and write it into the swap disk.
   The start_sector_index is passed by address in the
   second parameter.
   -- Return: If the swapping is successful, return true
              Otherwise false. */
bool
swap_out (struct frame* frame, size_t* start_sector_index)
{
  if(frame == NULL)
        return false;

  void * kernel_addr = frame->kernel_addr;

  if ( swap_block == NULL || bitmap == NULL)
    {
      PANIC("Swap initialization fail!");
      return false;
    }

  if (kernel_addr == NULL )
    {
      PANIC(" Frame is invalid!");
      return false;
    }

  /* Find a 8 free continuous sectors
     and mark them as used. */
  lock_acquire (&swap_lock);

  pagedir_clear_page (frame->frame_holder->pagedir,
                          frame->page_entry->virtual_addr);

  size_t start_sector;

  /* If this swap file has been in the swap disk,
     then we can skip the scanning. */
  if( *start_sector_index != BITMAP_ERROR)
    start_sector = *start_sector_index ;
  else
    start_sector = bitmap_scan (bitmap, 0, NM_SEC_PER_PG, UNUSED);

  if (start_sector == BITMAP_ERROR)
    {
      PANIC("Bitmap scanning fail!");
      return false;
    }

  bitmap_set_multiple (bitmap, start_sector, NM_SEC_PER_PG, USED);

  size_t i;
  int j = 0;

  /* Write the data out to the swap block. */
  for (i = start_sector; i < (NM_SEC_PER_PG + start_sector); i++)
    {
      block_write (swap_block, i, (kernel_addr + j * BLOCK_SECTOR_SIZE));
      j++;
    }

  /* Record the result. */
  *start_sector_index = start_sector;

  lock_release (&swap_lock);

  return true;
}

/* Swap in from the swap block.
   --kernel_addr: must be equal to pg_round_down(kernel_addr).
   --start_sector: index of the swap block in the swap disk. */
bool
swap_in (void* kernel_addr, size_t start_sector)
{
  if (kernel_addr == NULL)
    return false;

  /* Lazily initialize swap space. */
  if (swap_block == NULL)
    return false;

  lock_acquire (&swap_lock);

  size_t i;
  int j = 0;

  /* Read the data in from the swap block. */
  for (i = start_sector; i < (NM_SEC_PER_PG + start_sector); i++)
    {
      block_read (swap_block, i, (kernel_addr + j * BLOCK_SECTOR_SIZE));
      j++;
    }

  lock_release (&swap_lock);

  return true;
}

/* Free one page in the swap block at start_sector. */
void
swap_remove (size_t start_sector)
{
  lock_acquire (&swap_lock);
  bitmap_set_multiple (bitmap, start_sector, NM_SEC_PER_PG, UNUSED);
  lock_release (&swap_lock);
}

