#ifndef VM_SWAP
#define VM_SWAP

#include <stdbool.h>
#include "devices/block.h"
#include "threads/vaddr.h"
#include "vm/page.h"
#include "vm/frame.h"

/* number of sector for each page */
#define NM_SEC_PER_PG (PGSIZE / BLOCK_SECTOR_SIZE)

void swap_init (void);
bool swap_out(struct frame* frame, size_t* start_sector_index);
bool swap_in(void* kernel_addr, size_t start_sector);
void swap_remove(size_t start_sector);

#endif /* VM_SWAP */
